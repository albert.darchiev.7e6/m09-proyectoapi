package com.example
import com.example.models.*
import com.example.plugins.configureRouting
import com.example.plugins.configureSerialization
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.example.models.CustomPrincipal
import com.example.models.myRealm
import com.example.models.userTable
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*
import io.ktor.http.*
import io.ktor.server.auth.*
import io.ktor.server.plugins.cors.routing.*
import java.io.File

fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0", module = Application::module).start(wait = true)
}
fun Application.module() {
    configureSerialization()
    install(Authentication) {
        digest("myAuth") {
            realm = myRealm
            digestProvider { userName, _ ->
                userTable[userName]
            }
            validate { credentials ->
                if (credentials.userName.isNotEmpty()) {
                    CustomPrincipal(credentials.userName, credentials.realm)
                } else {
                    null
                }
            }
        }
    }
    install(CORS) {
        anyHost()
    }
    configureRouting()
    val file = File("./src/main/kotlin/com/example/watches.json")
    if (file.readText() != "") {
        getWatchesJson()
    }

}
fun getWatchesJson() {
    val mapper = jacksonObjectMapper()
    mapper.registerKotlinModule()
    val jsonString: String = File("./src/main/kotlin/com/example/watches.json").readText(Charsets.UTF_8)
    allWatches = mapper.readValue(jsonString)
}