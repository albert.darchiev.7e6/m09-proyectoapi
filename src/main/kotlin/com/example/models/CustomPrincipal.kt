package com.example.models

import User
import io.ktor.server.auth.*
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import java.io.File
import java.nio.charset.StandardCharsets.UTF_8
import java.security.MessageDigest

data class CustomPrincipal(val userName: String, val realm: String) : Principal

fun getMd5Digest(str: String): ByteArray = MessageDigest.getInstance("MD5").digest(str.toByteArray(UTF_8))

val myRealm = "Access to the '/' path"

var watchestable = mutableListOf<Watch>()
var userTable: MutableMap<String, ByteArray> = mutableMapOf()

//val usersWatches: MutableMap<String, MutableList<User>> = mutableMapOf()

//fun getUsers() {
//    userTable = User.generateClass(File("./src/main/kotlin/com/example/Users.json")).map {
//        it.map()
//    }.associate { it.first to it.second }.toMutableMap()
//}

fun getWatches(){
    val file = File("./src/main/kotlin/com/example/watches.json")
    val jsonString = file.readText()
    allWatches = Json.decodeFromString(jsonString)
    watchestable = allWatches

}