package com.example.models

import User.Companion.generateClass
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

@Serializable
data class Watch(
    val id: Int,
    val brand: String,
    val model: String,
    val price: Int,
//    @SerialName("imageURL") // Specify the custom name for the property
    val imageUrl: String,
    val gender: String,
    val seller: String
) {
//    init {
//        updateWatchesJson()
//    }

    fun addWatch() {
        val jsonfile = File("./src/main/kotlin/com/example/watches.json")
        val listWatches= Json.encodeToString(jsonfile.readText())

        val listwatchesMutable = listWatches
//        listwatchesMutable.add(this)
        jsonfile.writeText(Json.encodeToString(listwatchesMutable))
    }

    fun updateWatchesJson(){
        try {
            val file = File("src/main/kotlin/com/example/watches.json")
            val jsonString = file.readText()
            allWatches = Json.decodeFromString(jsonString)
            println("XXXXXXX "+ allWatches)
        }
        catch (e: Exception){println(e) }

    }
}

var allWatches = mutableListOf<Watch>()
