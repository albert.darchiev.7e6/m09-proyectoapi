import com.example.models.getMd5Digest
import com.example.models.myRealm
import kotlinx.serialization.Serializable
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

@Serializable
data class User(val username: String, val password: String) {
    fun map(): Pair<String, ByteArray> {
        return Pair(username, getMd5Digest("$username:$myRealm:$password"))
    }

    fun createUser() {
        val jsonfile = File("./src/main/kotlin/com/example/Users.json")
        var listusers = generateClass(jsonfile)
        var listusersmutable = listusers.toMutableSet()
        listusersmutable.add(this)
        jsonfile.writeText(Json.encodeToString(listusersmutable))
    }



    companion object{
        private val json = Json {
            ignoreUnknownKeys = true
        }

        fun generateClass(str: File): List<User> = json.decodeFromString(str.readText())
    }

}