package com.example.plugins

import com.example.models.CustomPrincipal
import com.example.routes.authRoutes
import com.example.routes.watchRoutes
import io.ktor.server.routing.*
import io.ktor.server.response.*
import io.ktor.server.application.*
import io.ktor.server.auth.*

fun Application.configureRouting() {
    routing {
        authRoutes()
        watchRoutes()

    }

    }

