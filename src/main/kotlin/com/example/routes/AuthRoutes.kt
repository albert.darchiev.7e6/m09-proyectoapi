package com.example.routes

import User
import com.example.models.*
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import java.io.File

fun Route.authRoutes(){
    route("/register") {
        post {
            val newUser = call.receive<User>()
            if (userTable.contains(newUser.username)) {
                call.respondText("User already exists", status = HttpStatusCode.BadRequest)
                return@post
            }
            else {
//                newUser.createUser()
                userTable[newUser.username] = getMd5Digest("${newUser.username}:$myRealm:${newUser.password}")
                addUsers()
                call.respondText("User registered successfully", status = HttpStatusCode.Created)
            }
        }
    }

    route("/login") {
        post {
            getUsers()
            val currentUser = call.receive<User>()
            val currentUserEncrypt = getMd5Digest("${currentUser.username}:$myRealm:${currentUser.password}")

            if (userTable.containsKey(currentUser.username) && userTable[currentUser.username]?.contentEquals(currentUserEncrypt) == true) {
                call.respondText("Login successful", status = HttpStatusCode.Accepted)
            }
            else {
                call.respondText("Invalid user credentials", status = HttpStatusCode.NotFound)
            }
        }
    }
}

fun getUsers(){
    val mapper = jacksonObjectMapper()
    mapper.registerKotlinModule()
    val jsonString: String = File("./src/main/kotlin/com/example/Users.json").readText(Charsets.UTF_8)
    userTable = mapper.readValue(jsonString)
}

fun addUsers(){
    val mapper = jacksonObjectMapper()
    mapper.registerKotlinModule()
    val jsonFile = File("./src/main/kotlin/com/example/Users.json")
    val json = mapper.writeValueAsString(userTable)
    jsonFile.writeText(json)
}