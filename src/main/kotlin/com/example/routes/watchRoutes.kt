package com.example.routes

import com.example.models.*
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.auth.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.*
import java.io.File

fun Route.watchRoutes(){

    authenticate("myAuth") {

    route("/watch") {
        get {
            getWatches()
            if (allWatches.isNotEmpty()) call.respond(allWatches.toList())
             else call.respondText("No customers found", status = HttpStatusCode.NotFound)
        }
        get("{id?}") {
            getWatches()
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText("Missing id", status = HttpStatusCode.BadRequest)
            val id = call.parameters["id"]
            for (watch in allWatches) {
                if (watch.id == id?.toInt()) return@get call.respond(watch)
            }
            call.respondText( "No watch with id $id", status = HttpStatusCode.NotFound)
        }
        post{
            val watch = call.receive<Watch>()
            allWatches.add(watch)
            call.respondText("S'ha afegit correctament", status = HttpStatusCode.Accepted)
            addWatchJson()
//            return@post

        }

        get("/my-watches") {
            val username = call.principal<CustomPrincipal>()!!.userName
            if (allWatches.isNotEmpty()) {
                val mywatches = mutableListOf<Watch>()
                for (watch in allWatches) {
                    if (watch.seller == username) mywatches.add(watch)
                }
                call.respond(mywatches)
            }
            else call.respondText("No tienes anuncios", status = HttpStatusCode.NotFound)
        }

        delete("{id?}"){
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]?.toIntOrNull()
            for(watch in allWatches){
                if (watch.id == id){
                    allWatches.remove(watch)
                    return@delete call.respondText("La pel·licula s'ha esborrat correctament", status = HttpStatusCode.Accepted)
                }
            }
        }
//        put("{id?}"){
//            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText("Missing id", status = HttpStatusCode.BadRequest)
//            if (allWatches.map { it.id }.contains(call.parameters["id"]?.toInt())){
//                allWatches.removeAll { it.id == call.parameters["id"]?.toInt() }
//                val watchupdate = call.receive<Watch>()
//                allWatches.add(watchupdate)
//                call.respondText("S'ha actualitzat el rellotge", status = HttpStatusCode.Accepted)
//            }
//
//    }
    }}
}
fun addWatchJson() {
    val mapper = jacksonObjectMapper()
    mapper.registerKotlinModule()
    val jsonFile = File("./src/main/kotlin/com/example/watches.json")
    val json = mapper.writeValueAsString(allWatches)
    jsonFile.writeText(json)
}